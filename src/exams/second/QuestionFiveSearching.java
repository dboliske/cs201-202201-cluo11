package exams.second;

import java.util.Scanner;

public class QuestionFiveSearching {
	
	public static int jumpSearch(double[] array, String value) {
		try{
			double searchValue = Double.parseDouble(value);
			
			//create jump search method
			int step = (int)Math.sqrt(array.length);
			int pos = 0;
			while (array[Math.min(step, array.length) - 1] - searchValue < 0) {
				pos = step;
				step += (int)Math.sqrt(array.length);
				if (pos >= array.length) {
					return -1;
				}
			}
			
			while (array[pos] - searchValue < 0) {
				pos++;
				if (pos == Math.min(step, array.length - 1)){
					return -1;
				}
			}
			
			if (array[pos] == searchValue) {
				return pos;
			}
		}catch (Exception e) {
			return -2;// create a flag for no-number input.
		}
		
		
		return -1;
	}

	public static void main(String[] args) {
		double[] number = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search value: ");
		String value  = input.nextLine();
		
		int index = jumpSearch(number, value);
		if (index == -1) {//input is a number but not present in this array
			System.out.println(index);
		}else if(index == -2) {//reminder when the input is not an number
			System.out.println("Sorry, invalid input. Please enter an valid number!");
		}else {
			System.out.println(value + " found at index of " + index +".");
		}
		
		input.close();
	}

}
