package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class QuestionThreeArrayLists {
	//create find minimum method using sorting first
	public static double findMin(ArrayList<Double> number) {
		boolean done = false;
		
		do {
			done = true;
			for(int i=0; i<number.size() - 1; i++) {
				if (number.get(i+1) - number.get(i) <0) {
					double temp = number.get(i+1);
					number.remove(i+1);
					number.add(i+1, temp);
					done =false;
				}
			}
			
		}while (!done);
		
		
		return number.get(0);
	}
	
	//create find maximum method without using sorting method 
	public static double findMax(ArrayList<Double> number) {
		double temp = number.get(0);
		
		for(int i=0; i<number.size() -1; i++) {
			if(number.get(i+1) - temp > 0) {
				temp =number.get(i+1);
			}
		}
		
		return temp;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Double> number = new ArrayList<Double> ();

		boolean done = false;
		
		try {		
		do {
			System.out.println("Please enter a number or enter 'done' to exit: ");
			String in = input.nextLine();
			if(!in.equalsIgnoreCase("done")) {
				double value = Double.parseDouble(in);
				number.add(value);

			}else if(in.equalsIgnoreCase("done") && number.size() == 0){
					System.out.println("Please enter at least one number to continue.");//to distinguish invalid number input and input "done" at the very beginning
			}else if(in.equalsIgnoreCase("done") && number.size() != 0) {
				done = true;
			}

		}while(!done);

		double min = findMin(number);
		double max = findMax(number);
		
		input.close();
		
		System.out.println("The minimum value is " + min + "\n"
						+ "The maximun value is " + max);
		
		}catch(Exception e) {
			System.out.println("That's not a valid number, exitting program...!");
		}
		
	}

}
