package exams.second;

public class Classroom {
	
	//attributes
	protected String building;
	protected String roomNumber;
	private int seats;
	
	//default constructor
	public Classroom() {
		building = "building";
		roomNumber= "roomNumber";
		seats = 1;
	}
	
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String number) {
		this.roomNumber = number;
	}
	
	public void setSeats(int seats) {
		if (seats > 0) {
		this.seats = seats;
		}
	}
	
	public String getBuilding() {
		return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	@Override
	public String toString() {
		return  "This classroom locates on " + building 
				+ " and the room number is " + roomNumber
				+ ", it has " + seats +" seats. ";
	}

}
