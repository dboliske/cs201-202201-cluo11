package exams.second;

public class QuestionFourSorting {
	
	public static String[] selectionSort(String[] array) {
		//outer loop
		for (int i=0; i< array.length - 1; i++) {
			int min = i;
			//inner loop to compare two string's alphabetically order
			for (int j=i+1; j<array.length; j++) {
				if (array[j].compareTo(array[min]) < 0) {
					min = j;//in wrong order assign min to String[] at j
				}
			}
			if (min != i) {//switch order when in wrong order
				String temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		words = selectionSort(words);
		
		//print out the sorted results
		for(String s : words) {
			System.out.print(s + " ");
		}
	}

}
