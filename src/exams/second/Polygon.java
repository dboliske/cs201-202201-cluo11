package exams.second;

public abstract class Polygon {
	
	protected String name;
	
	public Polygon() {
		name = "name";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "This is a " + name;
	}
	
	public abstract double area();
	public abstract double perimeter();

}
