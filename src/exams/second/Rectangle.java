package exams.second;

public class Rectangle extends Polygon {
	
	private double width;
	private double height;
	
	public Rectangle() {
		super();
		width = 1;
		height = 1;
	}
	
	
	public double getWidth() {
		return width;
	}

	
	public void setWidth(double width) {
		if(width > 0) {
		this.width = width;
		}
	}


	public double getHeight() {
		return height;
	}


	public void setHeight(double height) {
		if(height >0) {
		this.height = height;
		}
	}
	
	@Override
	public String toString() {
		return super.getName() + " is a rectangle";
	}


	@Override
	public double area() {
		return height *width;
	}

	@Override
	public double perimeter() {
		return 2.0 * (height + width);
	}

}
