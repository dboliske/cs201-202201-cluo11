package exams.first;

import java.util.Scanner;

public class QuestionOneDataType {

	public static void main(String[] args) {
		//create a scanner and prompt for an integer
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter an integer: ");
		//assign the input value to int i
		int i = Integer.parseInt(input.nextLine());
		int j = i +65;
		//print out the character of j
		System.out.println("The result is "+ (char)j);
			
		input.close();
	}

}
