# Midterm Exam

## Total

95/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  18/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  3/5
    - Formatting:               5/5
4. Arrays:                      19/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  4/5
5. Objects:                     20/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      5/5

## Comments

1. Good, but needs to handle non-integer input.
2. Good
3. Good, but needs to handle non-integer input.
4. Good, but doesn't consider when more than one word is repeated.
5. Perfect
