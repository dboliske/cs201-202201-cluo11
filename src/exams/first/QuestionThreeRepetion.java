package exams.first;

import java.util.Scanner;

public class QuestionThreeRepetion {

	public static void main(String[] args) {
		//create a scanner and prompt for an integer
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter an integer for a triangle: ");
		int in = Integer.parseInt(input.nextLine());
		
		for(int i=0; i<in; i++) {//outer looper for the row
			for(int j=0; j<in; j++) {
				if(j>=i) {//inner looper to print out * and space at target column
					System.out.print(" *");
				}else if(j<i) {
					System.out.print("  ");
				}
			}
			System.out.print("\n");
		}
		
		input.close();
	}

}
