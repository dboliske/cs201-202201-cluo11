package exams.first;

public class Pet {
	//attributes
	private String name;
	private int age;
	
	//default constructor
	public Pet() {
		name = "spot";
		age = 1;
	}
	
	//non-default constructor
	public Pet(String name, int age) {
		this.name = name;
		this.age = 1;
		setAge(age);
	}
	
	//mutator methods
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if(age >0) {
			this.age = age;
		}
	}
	
	//accessor methods
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	//boolean
	public boolean equals(Object obj) {
		if(!(obj instanceof Pet)) {
			return false;
		}
		
		Pet a = (Pet) obj;
		if(this.name != a.getName()) {
			return false;
		}else if (this.age != a.getAge()) {
			return false;
		}
		
		return true;
	}
	
	//toString
	public String toString() {
		return name +"is " + age +" years old.";
	}

}
