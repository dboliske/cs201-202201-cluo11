package exams.first;

import java.util.Scanner;

public class QuestionFourArrays {

	public static void main(String[] args) {
		//create a new scanner and new string array
		Scanner input = new Scanner(System.in);
		String[] words = new String[5];
		String temps = "temps";
		
		//assign the value into array from prompt
		for (int i=0; i<words.length; i++) {
			System.out.print("Enter (" + (i+1) + ") word: ");
			words[i] = input.nextLine();
		}	
		
		boolean repeat = false;//flag
		//See if any two element is exactly same
			for (int a=0; a<words.length; a++) {
				for (int b=a+1; b<words.length;b++) {
					if(words[a].equals(words[b])) {
						temps = words[a];
						repeat = true;			 			
					}
				}
			}
		//condition
		if(!repeat) {
			System.out.println("There is no any word appears more than once.");
		}else {
			System.out.println("The word that appears more that once is "  + temps);
		}
		
		input.close();
	}

}
