package exams.first;

import java.util.Scanner;

public class QuestionTwoSelection {

	public static void main(String[] args) {
		//create a new scannner and prompt for an integer
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter an integer: ");
		//create an integer and assign the value from user input
		int i = Integer.parseInt(input.nextLine());
		
		//condition
		if (i%2 == 0 && i%3 == 0) {
			System.out.println("foobar");//print out foobar when the integer is divisible by both
		}else if (i%2 ==0) {
			System.out.println("foo");//print out foo when the integer is divisible by 2
		}else if (i%3 == 0) {
			System.out.println("bar");//print out bar when the integer is divisible by 3
		}//do nothing when the integer is not divisible by either
		
		
		input.close();
	}

}
