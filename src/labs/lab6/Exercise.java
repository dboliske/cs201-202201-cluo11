package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercise {
	
	public static ArrayList<String> AddCustomer(ArrayList<String> data, Scanner input){

		System.out.println("New customer's name: ");
		String name = input.nextLine();
		data.add(name);

		return data;
	}
	
	public static ArrayList<String> HelpCustomer(ArrayList<String>data){
		data.remove(0);
		
		return data;
	}
	
	public static void ListCustomer(ArrayList<String>data){
			System.out.println(data);
		
	}
	
	public static ArrayList<String> menu(Scanner input, ArrayList<String> data){
		boolean done = false;
		
		String[] options = {"Add Customer", "Help Customer", "Exit", };
		
		do {
			for (int i=0; i<options.length;i++) {
				System.out.println((i+1) + ". " +options[i]);
			}
			System.out.println("Tell me what your want to do: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":
					data = AddCustomer(data, input);
					break;
				case "2":
					data = HelpCustomer(data);
					break;
				case "3":
					done = true;
					break;
				default:
					System.out.println("Sorry, I don't understand that.")	;
			}
			
		}while(!done);
		
		return data;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<String> data = new ArrayList<String>();
		//menu
		data = menu(input, data);

		input.close();
		System.out.println("Goodbye!");
	}

}
