package labs.lab4;

public class PhoneNumberApplication {

	public static void main(String[] args) {
		PhoneNumber p1 = new PhoneNumber();//use the default constructor
		
		p1.setCountryCode("1");//assign value
		p1.setAreaCode("312");
		p1.setNumber("0000000");
		System.out.println(p1.toString());//call the default constructor and print it out
		
		PhoneNumber p2 = new PhoneNumber("1", "312", "0000001");
		System.out.println(p2.toString());
	}

}
