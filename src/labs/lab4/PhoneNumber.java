package labs.lab4;

public class PhoneNumber {
	
	//edit the attributes
	private String countryCode;
	private String areaCode;
	private String number;
	
	//edit the operations
	//edit default constructors
	public PhoneNumber() {
		countryCode = "0";
		areaCode = "000";
		number = "0000000";
	}
	
	//edit non-default constructors
	public PhoneNumber(String cCode, String aCode, String n) {
		countryCode = cCode;
		areaCode = aCode;
		number = n;
	}
	
	//accessor methods
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	//mutator methods
	public void setCountryCode(String cCode) {
		this.countryCode = cCode;
	}
	
	public void setAreaCode(String aCode) {
		if (aCode.length() == 3) {
			this.areaCode = aCode;
		}
	}
	
	public void setNumber(String number) {
		if (number.length() == 7) {
		this.number = number;
		}
	}
	
	//toString method
	public String toString() {
		return "+" + countryCode + 
				"(" + areaCode + ")" + 
				number;
	}
	
	//validAreaCode method
	public boolean validAreaCode(String aCode) {
		if(aCode.length() != 3) {
			return false;
		}
		
		return true;
	}
	
	public boolean validNumber(String number) {
		if(number.length() != 7) {
			return false;
		}
		
		return true;
	}
	
	//equals method
	public boolean equals(PhoneNumber p) {
		if (this.countryCode != p.getCountryCode()) {
			return false;
		} else if (this.areaCode != p.getAreaCode()) {
			return false;
		}else if (this.number != p.getNumber()) {
			return false;
		}
		
		return true;
	}

}
