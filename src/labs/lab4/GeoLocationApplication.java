package labs.lab4;

public class GeoLocationApplication {

	public static void main(String[] args) {
		GeoLocation g1 = new GeoLocation();//use the default constructor
		
		g1.setLat(90);//assign the value for latitude
		g1.setLng(120);//assign the value for longitude
		System.out.println(g1.toString());//call the default constructor and print it out
		
		GeoLocation g2 = new GeoLocation(90, 100);//use the non-default constructor
		System.out.println(g2.toString());//print out the non-default constructor
	}

}
