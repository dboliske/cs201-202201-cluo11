package labs.lab4;

public class Potion {
	//edit the attributes
	private String name;
	private double strength;
	
	//edit the operations
	//default constructor
	public Potion() {
		name = "name";
		strength = 0.0;
	}
	
	//non-default constructor
	public Potion(String n, double s) {
		name = n;
		strength = s;
	}
	
	//accessor methods
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	//mutator methods
	public void setName(String name) {
		this.name = name;
	}
	
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
	// toString method
	public String toString() {
		return "The strength of " + name + " is " + strength;
	}
	
	//validStrength method
	public boolean validStrength(double strength) {
		if (strength < 0 || strength > 10) {
			return false;
		}
		
		return true;
	}
	
	//equals method
	public boolean equals(Potion p) {
		if (this.name != p.getName()) {
			return false;
		} else if (this.strength != p.getStrength()) {
			return false;
		}
		
		return true;
	}
}
