package labs.lab4;

public class PotionApplication {

	public static void main(String[] args) {
		Potion p1 = new Potion();//use the default constructor
		
		p1.setName("Potion of Immortality");//assign value
		p1.setStrength(9);
		System.out.println(p1.toString());//call the default constructor and print it out
		
		Potion p2 = new Potion("Potion of Immortality", 8);//use the non-default constructor
		System.out.println(p2.toString());//call the non-default constructor and print it out
	}

}
