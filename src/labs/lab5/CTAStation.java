package labs.lab5;

public class CTAStation extends GeoLocation{
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() {
		super();
		this.name = "Name";
		this.location = "Location";
		wheelchair = false;
		open = false;
	}
	
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
		super(lat, lng);
		this.name = name;
		this.location = location;
		this.wheelchair = wheelchair;
		this.open = open;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public boolean hasWheelchair() {
		return wheelchair;
	}

	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
	@Override
	public String toString() {
		return  name  +"that locate on " + location +", which geolocation is " + super.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		CTAStation c = (CTAStation)obj;
		if (!this.name.equals(c.getName())){
			return false;
		}else if (!this.location.equals(c.getLocation())) {
			return false;
		}else if (!super.equals(c)) {
			return false;
		}
		
		return true;
		
	}
}
