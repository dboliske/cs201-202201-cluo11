 package labs.lab5;

public class GeoLocation {
	
	//edit attributes
	private double lat;
	private double lng;
	
	//edit operations
	//default constructor
	public GeoLocation() {
		lat = 0.0;
		lng = 0.0;
	}
	
	//non-default constructor
	public GeoLocation(double lat, double lng) {
		this.lat = 0.0;
		setLat(lat);
		this.lng = 0.0;
		setLng(lng);
	}
	
	//accessor methods
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	
	//mutator methods
	public void setLat(double lat) {
		if (lat >= -90 && lat <= 90) {
			this.lat = lat;
		}
	}
	
	public void setLng(double lng) {
		if (lng>= -180 && lng <= 180) {
			this.lng = lng;
		}
	}
	
	//toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	//
	public boolean validLat(double lat) {
		if (lat <-90 || lat > 90) {
			return false;
		}
		return true;
	}
	
	public boolean validLng(double lng) {
		if (lng<-180 || lng>180) {
			return false;
		}
		return true;
	}
	
	//equals method
	public boolean equals(GeoLocation g) {
		if (this.lng != g.getLng()) {
			return false;
		}else if (this.lat != g.getLat()) {
			return false;
		}
		
		return true;
	}
	
	//calcDistance method
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(lat, 2) + Math.pow(lng, 2));
	}
	
	public double calcDistance(double lat1, double lng1, double lat2, double lng2) {
		return Math.sqrt(Math.pow(lat1 - lat2, 2) + Math.pow(lng1-lng2, 2));
	}

}
