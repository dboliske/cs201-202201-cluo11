package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class CTAStopApp {


	public static CTAStation[] readFile(String filename) {

		//readFile
		CTAStation[] stations = new CTAStation[10];
		int count = 0;

		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);//Name,Latitude,Longitude,Location,Wheelchair,Open
		
			while(input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					
					CTAStation c = null;
					if(!values[1].equals("Latitude")) {//Start read data from the second line
					c = new CTAStation(values[0], 
							Double.parseDouble(values[1]),
							Double.parseDouble(values[2]),
							values[3],
							Boolean.parseBoolean(values[4]),
							Boolean.parseBoolean(values[5])
							);
					if (stations.length == count) {
						stations = resize(stations, stations.length*2);
					}
					
					stations [count] = c;
					count++;
					}
				}catch(Exception e) {
					System.out.println(e.getMessage());
				}
			}
			input.close();

		}catch (FileNotFoundException fnf) {
			
		}catch (Exception e) {
			System.out.println("Error occurred when reading in file.");
		}
		stations = resize(stations, count);
		return stations;
	}
	
	public static CTAStation[] resize(CTAStation[] stations, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = stations.length > size ? size : stations.length;
		for (int i= 0; i<limit; i++) {
			temp[i]= stations[i];
		}
		return temp;
	}
 
		//menu
	public static CTAStation[] menu(Scanner input, CTAStation[] stations) {
		boolean done = false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Station with/without Wheelchair access");
			System.out.println("3. Dsplay Nearest Station");
			System.out.println("4. Exit");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			
			switch(choice) {
				case "1"://displayStationnames
					displayStationNames(stations);
					break;
				case "2":
					displayByWheelchair(stations, input);
					break;
				case "3":
					displayNearest(stations, input);
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("Sorry, that's not an option!");
			}	
		}while(!done);
		return stations;
	}

		//displayStationNames
	public static void displayStationNames(CTAStation[] stations) {
		for (int i=0; i<stations.length; i++) {
			System.out.println(stations[i].getName());
		}
		
	}
	
	//displayByWheelchair 
	public static void displayByWheelchair (CTAStation[] stations, Scanner input) {		
		boolean[] wheelchair = new boolean[stations.length];
		for(int i=0; i<stations.length; i++) {
			wheelchair[i]=stations[i].hasWheelchair();
		}
		
		boolean done = false;
		
		do {			
			System.out.println("Prompt for user accessibility ('y' or 'n'): ");
			String yn = input.nextLine();

			switch (yn.toLowerCase()) {
				case "y":
					System.out.println("The CTAStations that has wheelchair available are: ");
					
					try{
						for(int i=0; i<stations.length; i++) {
					
						if (wheelchair[i] == true) {
							System.out.println(stations[i]);
						}
					}
					}catch(Exception e) {
						System.out.println("Sorry, no required CTAStation found.");
					}
					done = true;
					break;
				case "n":
					System.out.println("The CTAStations that doesn't have wheelchair available are: ");
					
					try {			
					for(int i=0; i<stations.length; i++) {
						if (wheelchair[i] == false) {
							System.out.println(stations[i]);
						}
					}
					}catch(Exception e) {
						System.out.println("Sorry, no required CTAStation found.");
					}
					done = true;
					break;
				default:
					System.out.println("That is not an option! Please enter the valid choice!");	
			}

		}while(!done);
		//return stations;
	}
	
	//displayNearest
	public static void displayNearest(CTAStation[] stations, Scanner input) {
		System.out.println("Please enter latitude of your location: ");
		Double a = Double.parseDouble(input.nextLine());
		System.out.println("Please enter longitude of your location: ");
		Double n = Double.parseDouble(input.nextLine());
		
		Double[] distance = new Double[stations.length];
		int count = 0;
		
		for(int i=0; i<stations.length; i++) {
			GeoLocation g= new GeoLocation();
			g.setLat(a - stations[i].getLat());
			g.setLng(n - stations[i].getLng());
			distance[i] = g.calcDistance(g);
			
		}
		
		for(int i=0; i<stations.length; i++) {//calculate the distance and find the closest one
			double mindistance = distance[0];
				if (mindistance < distance[i]) {
					mindistance = distance[i];
					count = i;
				}
		}
		System.out.println("The nearest station is " + stations[count].getName());
		//return stations;
	}
	
	//main
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Read file...");
		String filename = "src/labs/lab5/CTAStops.csv";
		CTAStation[] stations = readFile(filename);
		menu(input, stations);
		
		input.close();
		System.out.println("Goodbye!");
	}
	
	
}
