package labs.lab7;

import java.util.Scanner;

public class ExerciseFour {
	
	public static int search(String[] array, String value) {//create BinarySearch method
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		
		while (!found && start != end) {
			int mid = (start + end)/2;
			if (array[mid].equalsIgnoreCase(value)) {
				found = true;
				pos = mid;
			}else if (array[mid].compareToIgnoreCase(value) < 0 ) {
				start = mid +1;
			}else {
				end = mid;
			}
		}
		
		return pos;
	}

	public static void main(String[] args) {
		String [] lang = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input =  new Scanner(System.in);
		System.out.print("Enter the search term: ");
		String value = input.nextLine();
		int index = search(lang, value);
		if (index == -1) {
			System.out.println("Sorry, but the "+ value + " is not found.");
		}else {
			System.out.println(value + " found at index of " + index + ".");
		}
		
		input.close();
	}

}
