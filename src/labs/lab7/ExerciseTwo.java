package labs.lab7;

public class ExerciseTwo {

	public static String[] sort(String [] array) {//Create a InsertionSort method
		for(int i=0; i<array.length - 1; i++) {
			int j = i;
			while (j > 0 && array[j].compareTo(array[j-1]) < 0) {
				String temp = array[j];//switch the position between j and i
				array[j] = array[j-1];
				array[j-1] = temp;
				j--;
			}
		}
		
		return array;
	}
	
	public static void main(String[] args) {
		String [] obj = {"cat", "fat", "dog", "apple", "bat", "egg"};
		obj = sort(obj);//apply the sort method
		
		for(String o : obj) {
			System.out.print(o + " ");//print out the sorted array
	}
	}

}
