package labs.lab7;

public class ExerciseOne {


	private static Integer[] sort(Integer[] array) {//create a BubbleSort method
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<array.length - 1; i++) {
				if(array[i+1] - (array[i]) < 0) {//check if the integer at "i+1" is less than the integer at "i"
					int temp = array[i+1];//switch the position between i+1 and i
					array[i+1] = array[i];
					array[i] = temp;
					done = false;
				}
			}
		}while (!done);
		
		return array;
	}
	
	
	public static void main(String[] args) {
		Integer[] numbers = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		numbers = sort(numbers);//apply the sort method
		
		for (int i : numbers) {
			System.out.print(i +" ");//print out sorted integers array
		}
	}

}
