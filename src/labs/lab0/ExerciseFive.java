package labs.lab0;

import java.util.Scanner;

public class ExerciseFive {// to get a hollow square with '*'
	
	static void print_squaredi(int k) {//print the square
		int a, b;//define the primary and secondary diagonal
		
		for (a = 1; a <= k; a++) {//outer loop for rows
			
			for (b = 1; b <= k; b++) {//inner loop
				
				if (a == 1 || a == k || b == 1 || b == k)
						
						System.out.print("*");// print the star character on the side point
				else
					System.out.print(" ");// print the white spaces inside
			}
			System.out.println();//start from the next row
		}
	}

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);// create a scanner
		
		System.out.print("Please Enter the Length of Square: ");//prompt for the length of square
		
		int rows;
		rows = input.nextInt();//assign the value of rows
		
		print_squaredi(rows);//print the square with diagonal
		
		input.close();
	}

}