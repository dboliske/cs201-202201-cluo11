package labs.lab2;

import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
		Scanner input =  new Scanner(System.in);//create a new scanner
		
		boolean done = false;//flag control variable
		int i=0;
		double tot =0.0;
		 do {
			 System.out.println("Please enter the grade: ");//prompt for the grade input
			 System.out.println("Enter -1 if finished.");
			 double grade = Double.parseDouble(input.nextLine());
			 i++;
			 if (grade != -1){ //condition
				 tot = tot + grade;
			     double ave = tot/i;
			     System.out.println("The average is: " + "\n" +ave);
			 }
			 else {
				 done = true;//change flag variable
			 }
		}while(!done);//exit loop
		System.out.println("Goodbye!");
		input.close();
	}

}
