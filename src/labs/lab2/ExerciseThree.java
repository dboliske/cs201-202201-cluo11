package labs.lab2;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner and read user input
		
		boolean done = false; // flag control variable
		do {
			System.out.println("1. Say Hello");
			System.out.println("2. Addition");
			System.out.println("3. Multiplication");
			System.out.println("4. Exit");
			System.out.print("Option: ");
			String option = input.nextLine(); // get user's choice
			
			switch (option) {
				case "1":
					System.out.println("Hello" + "\n");
					break;
				case "2":
					System.out.println("Enter the 1st number: ");
					double i = Double.parseDouble(input.nextLine());//read user input (string) and convert it to double
					System.out.println("Enter the 2nd number: ");
					double j = Double.parseDouble(input.nextLine());//read user input (string) and convert it to double
					System.out.println("The sum of the two numbers is: " + (i+j) +"\n");//output the sum
					break;
				case "3":
					System.out.println("Enter the 1st number: ");
					double k = Double.parseDouble(input.nextLine());//read user input (string) and convert it to double
					System.out.println("Enter the 2nd number: ");
					double l = Double.parseDouble(input.nextLine());//read user input (string) and convert it to double
					System.out.println("The product of the two numbers is: " + (k*l) +"\n");//output the result
					break;
				case "4":
					done = true;//flag variable change
					break;
				default:
					System.out.println("That is not a valid option." + "\n");
			}
		} while (!done);
		
		input.close();
		
		System.out.println("Goodbye!");
	}

}
