package labs.lab2;

import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//Create a new scanner
		System.out.print("Please enter the size of square: ");//prompt for the size
		int size = Integer.parseInt(input.nextLine());//read in string and convert to integer
		
		for (int i=0; i<size; i++) {//outer loop 
			for (int j=0; j<size; j++) {//inner loop
				System.out.print("* ");//print out the * at row side
			}
			System.out.println();//nextline
		}
		
		input.close();
	}

}
