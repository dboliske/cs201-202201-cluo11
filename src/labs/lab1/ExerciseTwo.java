package labs.lab1;

import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Create a new scanner
		
		System.out.print("Please enter your age: "); //Prompt for the age
		int x = Integer.parseInt(input.nextLine());// Create a integer x and assign value from prompt
		
		System.out.print("Please enter your father's age: "); //Prompt for the father's age
		int y = Integer.parseInt(input.nextLine());// Create a integer y and assign value from prompt
		
		System.out.println("Your age subtracted from your fater's age: "+ (y - x));//Print out the age from age subtracted from father's age
		
		System.out.print("Please enter your birth year: ");//Prompt for the birth year
		int z = Integer.parseInt(input.nextLine());//Create a integer z and assign value from prompt
		
		System.out.println("Your birth year multiplied by 2: " + (z * 2));//Print out the year multiplied by 2
		
		System.out.print("Please enter your height in inches: ");//Prompt for the height in inches
		double a = Double.parseDouble(input.nextLine());//Create a double a and convert string from prompt to number and assign the value to z
		
		System.out.println("Your height in cms: " + (a * 2.54) + " cms");//Print out the height in cms
		
		System.out.print("Please enter your height in inches: ");//Prompt for the height in inches
		int b = Integer.parseInt(input.nextLine());//Create a integer b and convert string from prompt to number and assign it to b
		
		System.out.println("Your height in cms: " + (b / 12) + " feet");//Print out the height in feet
		
		input.close();
	}

}
