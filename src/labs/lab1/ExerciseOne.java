package labs.lab1;

import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);// create a new Scanner
		
		System.out.print("Enter a name: ");// prompt for a name from user
		
		String name = input.nextLine();// save the input
		
		System.out.println("Echo: " + name);// echo back the input
		
		input.close();

	}

}
