package labs.lab1;

import java.util.Scanner;

public class ExerciseSix {

	public static void main(String[] args) {
		Scanner input =  new Scanner(System.in);//Create a new scanner
		
		System.out.print("Please enter a length in inches: ");//Prompt for the inches
		double x = Double.parseDouble(input.nextLine());//Create double x and assign the value from prompt
		
		System.out.println("The length in cms: " +  (x * 2.54) + " cms.");//Convert inches to cms and print out the result
		
		input.close();	
	}
}
//TEST TABLE:
//       |    Input    |     Expected output     |    Actual output
//Low    |    2        |        5.08             |    5.08
//High   |    10000    |        25400            |    25400.0
//Middle |    120      |        304.8            |    304.8
//Test running good. Actual output is the same as the expected output.