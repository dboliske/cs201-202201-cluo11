package labs.lab1;

import java.util.Scanner;

public class ExerciseFive {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//Create a new scanner
		
		System.out.print("Please enter the length in inches of a box: ");//Prompt for the length in inches of a box
		double x = Double.parseDouble(input.nextLine());//Create a double x and assign the value from prompt
		
		System.out.print("Please enter the width in inches of a box: ");//Prompt for the width in inches of a box
		double y = Double.parseDouble(input.nextLine());//Create a double y and assign the value from prompt
		
		System.out.print("Please enter the depth in inches of a box: ");//Prompt for the depth in inches of a box
		double z = Double.parseDouble(input.nextLine());//Create a double z and assign the value form prompt
		
		double SF = (x * y + y * z + z * x) * 2;//Create a double SF to get the surface area of the box in square inches
		System.out.println("The amount of wood needed to make the box: " +  (SF / 144) + " square feet.");//Convert the SF from square inches to square feet and print it out
		
		input.close();
	}

}
//TEST TABLE:
//       |    Input(length/width/depth)    |    Expected output    |   Actual output
//Low    |       (3/4/5)                   |  0.6527777777777778   |0.6527777777777778
//High   |       (100/120/150)             |  625                  |625.0
//Middle |       (10/20/30)                |  15.277777777777778   |15.277777777777779
//There is a tiny different on the test at middle number.