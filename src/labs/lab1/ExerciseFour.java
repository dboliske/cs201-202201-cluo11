package labs.lab1;

import java.util.Scanner;

public class ExerciseFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//Create a new scanner
		
		System.out.print("Please enter a temperature in Fahrenheit: ");//Prompt for a temperature in Fahrenheit
		double x = Double.parseDouble(input.nextLine());//Create a double x and assign the value from prompt
		
		System.out.println("The temperature in Celsius: " + ((x - 32)/1.8));//Print out the temperature in Celsius
		
		System.out.print("Please enter a temperature in Celsius: ");//Prompt for a temperature in Celsius
		double y = Double.parseDouble(input.nextLine());//Create a double y and assign the value from prompt
		
		System.out.println("The temperature in Fahrenheit: " + (y * 1.8 + 32));//Print out the temperature in Fahrenheit
		
		input.close();

	}

}
//Test table:
//       |    Input    |             Expected output               |              Actual output
//Low    |-10   /-10   |-23.333333333333333  / 14                  |-23.333333333333332  / 14.0
//High   |10000 /10000 |5537.777777777778  /18032                  |5537.777777777777  /18032.0
//Middle |100   /100   |37.77777777777778  /212                    |37.77777777777778   /212.0
//Yea, I think the result works as expected, and it is good enough.