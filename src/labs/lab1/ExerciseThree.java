package labs.lab1;

import java.util.Scanner;

public class ExerciseThree {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create a new scanner
		
        System.out.print("Please enter a first name: ");//Prompt for a first name
		
		char c = input.nextLine().charAt(0);//Create a char c and assign it as the 1st letter from prompt
		
		System.out.println("The inital is " + c );//Print out char c
		
		input.close();
	}

}
