package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseOne {

	public static void main(String[] args) throws IOException {
	    File f = new File("src/labs/lab3/grades.csv");//Create a new file
	    Scanner input = new Scanner(f);//Create a new scanner
	    
	    double[] grades = new double[2];//Create a arrays
	    int count = 0;
	    
	    while(input.hasNextLine()) {
	    	String line = input.nextLine();
	    	String[] value = line.split(",");//read the value from","
	    	if (count == grades.length) {
	    		double[] bigger = new double[2 * grades.length];
	    		for (int i=0; i<grades.length; i++) {
	    			bigger[i] = grades[i];
	    		}
	    		grades = bigger;
	    		bigger = null;
	    	}
	    	grades[count] = Double.parseDouble(value[1]);//assign the value from file to arrays
	    	count++;
	    }
	    
	    input.close();
	    //trim array down
	    double[] smaller = new double[count];
	    for (int i=0; i<smaller.length; i++) {
	    	smaller[i] = grades[i];
	    }
	    grades = smaller;
	    smaller = null;
	    
	    Double total = 0.0;
	    for(int i=0; i<grades.length; i++) {
	    	total = total + grades[i];//Calculate the total grades
	    }
	    System.out.println("Average is: " + total/grades.length);//Print out the average
	}

}
