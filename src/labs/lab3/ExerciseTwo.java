package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ExerciseTwo {

	public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //create scanner for user input

        
		// create array
		int[] numbers = new int[2];
		int count = 0;
		boolean running =false;
		
		try {
			do  {
				try {
					System.out.println("Enter 'Done' if finised.");
					System.out.println("Please enter the(" + (count+1) + ")number: ");
					String in = input.nextLine();

					//maybe need to see if the input is a number
					if(!in.equalsIgnoreCase("done")) {
						if (count == numbers.length) {
							// resize the array
							int[] bigger = new int[2 * numbers.length];
							for (int i=0; i<numbers.length; i++) {
								bigger[i] = numbers[i];
							}
							numbers = bigger;
							bigger = null;
						}
						numbers[count] = Integer.parseInt(in);
						count++;
					}else{		
						running = true;
					}
				}
				catch(NumberFormatException e) {
					System.out.println("Invalid input!");
				}
			}while(!running);
			
		// trim array down
			int[] smaller = new int[count];
			for (int i=0; i<smaller.length; i++) {
				smaller[i] = numbers[i];
			}
			numbers = smaller;
			smaller = null; 
		
	
		//prompt for file's name and create a file
			System.out.print("Please enter the file's name: ");
			String name = input.nextLine();
		
		
			FileWriter f = new FileWriter("src/labs/lab3/"+ name);
			
			for (int j= 0; j<numbers.length;j++) {
				f.write(numbers[j] + "\n");
			}
				f.flush();
				f.close();	
				input.close();	
		}catch(IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Finished Writing, goodbye!");

	}
	}
