package project;
//Chunxin Luo  04/29/2022  create main body for the application include all the function required
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class StockApplication {
	
	public static ArrayList<Items> readData(String filename) {
		ArrayList<Items> items = new ArrayList<Items>();
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			//construct Items
			while(input.hasNextLine()) {
				try {
					String[] itemsInfo = input.nextLine().split(",");
					Items it = null;
					String name = itemsInfo[0];
					double price = Double.parseDouble(itemsInfo[1]);
					
					if(itemsInfo.length == 2) {
						it = new ShelvedItem(name, price);
						items.add(it);
					}else if (itemsInfo.length == 3 && itemsInfo[2].contains("/")){
						String[] typeInfo = itemsInfo[2].split("/");
						int i = Integer.parseInt(typeInfo[0]);
						int j = Integer.parseInt(typeInfo[1]);
						int k = Integer.parseInt(typeInfo[2]);
						int year = k;
						int month = i;
						int day = j;
						it = new ProduceItem(name, price, year, month, day);
						items.add(it);
					}else if (itemsInfo.length == 3 && !itemsInfo[2].contains("/")) {
						int age = Integer.parseInt(itemsInfo[2]);
						it = new AgeRestrictedItem(name, price, age);
						items.add(it);
					}			
				}catch (Exception e) {
					System.out.println("Error in reading data.");
				}
			}
			
			input.close();
			}catch (FileNotFoundException e) {
				System.out.println("Error! File  Not Found! Please exit and reload file again.");
			} catch (Exception e) {
				System.out.println("Error reading in data file.");
			}
		return items;
	}
	//create list method to allow myself to see change during testing process
	public static void listItem(ArrayList<Items> stock) {
		for (Items i : stock) {
			System.out.println(i);
		}
	}
	
	public static ArrayList<Items> menu(ArrayList<Items> stock){
		Scanner input = new Scanner(System.in);
		
		boolean done = false;//create a flag;
		
		do {
			//print out the user menu and prompt for user option
			System.out.println("1. Create a new items");
			System.out.println("2. Sell items");
			System.out.println("3. Search for an item in the store");
			System.out.println("4. Modify items' information");
			System.out.println("5. Exit");

			System.out.print("Choice: ");
			String in = input.nextLine();
			
			switch (in) {
				case "1"://create
					stock = createItem(stock, input);
					break;
				case "2"://sell
					stock = sellItem(stock, input);
					break;
				case "3"://search
					Items i = searchItem(stock, input);
					if (i == null) {
						//System.out.println("Sorry, it is not available.");
					}else{
						System.out.println(i.toString());
					}
					break;
				case "4"://modify
					stock = modifyItem(stock, input);
					break;
				case "5"://exit
					System.out.println("Do you want to sort and save change (y/n).");//user decide whether the changed need to be save
					String yn = input.nextLine();
					switch(yn.toLowerCase()) {
						case "y":
						case "yes":
							saveFile(stock, input);//yes, then save to file and exit
							done = true;
							break;
						case "n":
						case "no"://no, then exit directly
							done = true;
							break;
							default:
								System.out.println("Sorry, I don't understand.");
					}
					break;
				default:
					System.out.println("I'm sorry, I don't understand that. Please try again");
			}
			
		}while(!done);
		
		input.close();
		System.out.println("Goodbye!");
		
		return stock;
	}
	
	public static ArrayList<Items> createItem(ArrayList<Items> stock, Scanner input){
		
		Items i= null;
		System.out.println("Please enter the item's name: ");//prompt for the item's name;
		String name = input.nextLine();
		System.out.println("Please enter the item's unit price: ");//prompt for the price
		double price = 0.0;
		try {
		price = Double.parseDouble(input.nextLine());
		
		} catch (Exception e) {//make sure the enter is a double
			System.out.println("That is not a valid price. Returning to menu");
			return stock;
		}
		
		System.out.println("Please enter the item's type (Produce, Shelved, or AgeRestricted): ");
		String type = input.nextLine();
		switch (type.toLowerCase()) {//prompt for the item's type
			case "produce":
				System.out.print("Please enter the expiration date of items (xx/xx/xxxx): ");
				int year = 0;
				int month = 0;
				int day = 0;
				String [] expiredDate = input.nextLine().split("/");
				try{
					year = Integer.parseInt(expiredDate[2]);
					month = Integer.parseInt(expiredDate[0]);
					day = Integer.parseInt(expiredDate[1]);
									
				}catch (Exception e) {
					System.out.print("Sorry, that's not a valid date. Returning to menu.");
					return stock;
				}
				
				i = new ProduceItem(name, price, year, month, day);
				break;
				
			case "shelved":			
				i = new ShelvedItem(name, price);
				break;
				
			case "agerestricted":
				System.out.print("Please enter restricted age: ");
				int age = 0;
				try {//make sure the enter is an integer
					age = Integer.parseInt(input.nextLine());
					
				i = new AgeRestrictedItem(name, price, age);
				}catch (Exception e) {
					System.out.print("That is no a valid number. Returning to menu.");
					return stock;
				}
				break;
				default:
					System.out.println("Please enter valid item's type. Returning to menu.");
					return stock;
				
		}
		//add the amount of the items
		System.out.print("Please enter the amount of the new items: ");
		try {
			int amount = Integer.parseInt(input.nextLine());
			for(int j=1; j<=amount; j++) {
				stock.add(i);
			}
			System.out.println(amount + " * " + i.toString() + " was added.");
			
		}catch (Exception e) {
			System.out.print("Sorry, that's not a valid number. Returning to menu. ");
			return stock;
		}
		
		return stock;
	}
	
	//sell items
	public static ArrayList<Items> sellItem(ArrayList<Items> stock, Scanner input){
		ArrayList<Items> toSell = new ArrayList<Items>();
		ArrayList<String> name = new ArrayList<String>();
		boolean done = false;
		int amount=0;
		double totalPrice = 0.0;
		Items flag = new Items("name", 0.0);
		int count = 0;
		try {
			do {
			
					Items it = searchItem(stock, input);
					if(it == null) {
						//System.out.println("Sorry, it is not available.");
					}else if (it.equals(flag)) {
						System.out.println("Total price: " + totalPrice + "\n"
						+ "Thank you for shopping today.");
				        done = true;
					}else{
						
						System.out.println("How many do you want?");
						amount = Integer.parseInt(input.nextLine());			
						int itemAmount = countItem(stock, it.getName());
						totalPrice = 0.0;//reset the total price to 0
						
							if(amount <0) {
								System.out.println("Sorry, that's not a valid number");
							}else if (amount > itemAmount) {
								System.out.println("Sorry, we don't have enougn " + it.getName() +" left today.");
							}else if (amount <= itemAmount && amount >0) {
							for (int i=0; i<amount; i++){
							     toSell.add(it);
							}
						//display items in the cart
						System.out.println("Your cart: ");
						for(Items i : toSell) {
							totalPrice += i.getPrice();
						}
						name.add(it.getName());
						for(int i=0; i<=count;i++) {
						System.out.println(name.get(i) + " * " + amount);
						}
						System.out.println("Enter 'checkout' when you finish!");
						count++;
						}
						
				}
			}while(!done);
		}catch (Exception e) {
			System.out.println("Sorry, that's not an number. Returning to menu.");
			return stock;
		}
		
		//remove sold items in stock
		for (Items i : toSell) {
			stock.remove(i);
		}
		
		return stock;
	}
	
	//count the amount of items in stock
	public static int countItem(ArrayList<Items> stock, String in) {
		int amount = 0;
		ArrayList<Items> target = new ArrayList<Items>();
		
		for (Items i : stock) {//sequential search items i
			if (in.equalsIgnoreCase(i.getName())) {
				target.add(i);
				amount += 1;
			}
		}
		return amount;
	}
	
	//search item
	public static Items searchItem(ArrayList<Items> stock, Scanner input) {
		System.out.print("Please enter the item's name: ");
		String name = input.nextLine();
		ArrayList<Items> target = new ArrayList<Items>();
		Items flag = new Items("name",0.0);
		
		for(Items i : stock) {
			if(name.equalsIgnoreCase(i.getName())) {
				target.add(i);
			}
		}
		
		if (target.size() == 0 && !name.equalsIgnoreCase("checkout")) {
			System.out.println("Sorry, it is not available.");
		return null;
		
		}else if(target.size() == 0 && name.equalsIgnoreCase("checkout")) {
		return flag;//create a flag here to allow check out process when do the 'sellItem' method
		
		}
		else if (target.size() > 0) {
			System.out.println("We got " + target.get(0).getName() + " * " + target.size()+ " available in stock.");
			
		}
		return target.get(0);
	}
	
	//modify item's info
	public static ArrayList<Items> modifyItem(ArrayList<Items> stock, Scanner input){
		Items it = searchItem(stock, input);
		
		if(it instanceof ProduceItem) {//apply modifyProduceItem method
			stock = modifyProduceItem(stock, input, it);

		}else if (it instanceof ShelvedItem) {//apply modifyShelvedItem method
			stock = modifyShelvedItem(stock, input, it);
			
		}else if (it instanceof AgeRestrictedItem) {//apply modify AgeRestrictedItem method
			stock = modifyAgeRestrictedItem(stock, input, it);
		}
		
		
		return stock;
	}
	//create modify produce item method
	public static ArrayList<Items> modifyProduceItem(ArrayList<Items> stock, Scanner input, Items it){
		boolean done = false;
		try {
			do {
				ProduceItem p = (ProduceItem) it;
				System.out.println("What infomation do you want to modify?");
				System.out.println("1. name");
				System.out.println("2. price");
				System.out.println("3. experation date");
				System.out.println("4. finsh");
				System.out.println("Choice: ");
				String in = input.nextLine();
						
				switch (in.toLowerCase()) {
					case "1":
						System.out.println("Current Name: " + p.getName());
						System.out.print("New name: ");
						String newname = input.nextLine();
						stock = replaceName(stock, p.getName(), newname);
						p.setName(newname);
						System.out.println("Modified items: " + p.toCSV());//show the modified item's info
						break;
					case "2":
						System.out.println("Current price: " + p.getPrice());
						System.out.print("New price: ");
						double newprice = Double.parseDouble(input.nextLine());
						stock = replacePrice(stock, p.getName(), newprice);
						p.setPrice(newprice);
						System.out.println("Modified items: " + p.toCSV());
						break;
					case "3":
						System.out.println("Current expiration date: " + p.dateToString());
						System.out.print("New date(xx/xx/xxxx): ");
						String newdate = input.nextLine();
						String[] date = newdate.split("/");
						stock = replaceDate(stock, p.getName(), newdate);
						p.setMonth(Integer.parseInt(date[0]));
						p.setDay(Integer.parseInt(date[1]));
						p.setYear(Integer.parseInt(date[2]));
						System.out.println("Modified items: " + p.toCSV());
						break;
					case "4":
						done = true;
						break;
					default:
						System.out.println("Not an option!");	
				}
				
				}while(!done);
		}catch (Exception e) {
			System.out.println("That's not a valid input. Returning to menu");
		}
		return stock;
	}
	//create modify shelved item method
	public static ArrayList<Items> modifyShelvedItem(ArrayList<Items> stock, Scanner input, Items it){
		boolean done = false;
		try {
			do {
				ShelvedItem p = (ShelvedItem) it;
				System.out.println("What infomation do you want to modify?");
				System.out.println("1. name");
				System.out.println("2. price");
				System.out.println("3. finsh");
				System.out.println("Choice: ");
				String in = input.nextLine();
						
				switch (in.toLowerCase()) {
					case "1":
						System.out.println("Current Name: " + p.getName());
						System.out.print("New name: ");
						String newname = input.nextLine();
						stock = replaceName(stock, p.getName(), newname);
						p.setName(newname);
						System.out.println("Modified items: " + p.toCSV());
						break;
					case "2":
						System.out.println("Current price: " + p.getPrice());
						System.out.print("New price: ");
						double newprice = Double.parseDouble(input.nextLine());
						stock = replacePrice(stock, p.getName(), newprice);
						p.setPrice(newprice);
						System.out.println("Modified items: " + p.toCSV());
						break;
					case "3":
						done = true;
						break;
					default:
						System.out.println("Not an option!");	
				}
				
				}while(!done);
		}catch (Exception e) {
			System.out.println("That's not a valid input. Returning to menu");
		}
		return stock;
	}
	//create modify age restricted item method
	public static ArrayList<Items> modifyAgeRestrictedItem(ArrayList<Items> stock, Scanner input, Items it){
		boolean done = false;
		
		try {
		do {
			AgeRestrictedItem p = (AgeRestrictedItem) it;
			System.out.println("What infomation do you want to modify?");
			System.out.println("1. name");
			System.out.println("2. price");
			System.out.println("3. restricted age");
			System.out.println("4. finsh");
			System.out.println("Choice: ");
			String in = input.nextLine();
					
			switch (in.toLowerCase()) {
				case "1":
					System.out.println("Current Name: " + p.getName());
					System.out.print("New name: ");
					String newname = input.nextLine();
					stock = replaceName(stock, p.getName(), newname);
					p.setName(newname);
					System.out.println("Modified items: " + p.toCSV());
					break;
				case "2":
					System.out.println("Current price: " + p.getPrice());
					System.out.print("New price: ");
					double newprice = Double.parseDouble(input.nextLine());
					stock = replacePrice(stock, p.getName(), newprice);
					p.setPrice(newprice);
					System.out.println("Modified items: " + p.toCSV());
					break;
				case "3":
					System.out.println("Current restricted age: " + p.getAgeRestriction());
					System.out.print("New restricted age: ");
					String age = input.nextLine();
					int newage = Integer.parseInt(age);
					stock = replaceAge(stock, p.getName(), newage);
					System.out.println("Modified items: " + p.toCSV());
					break;
				case "4":
					done = true;
					break;
				default:
					System.out.println("Not an option!");	
			}
			
			}while(!done);
		}catch (Exception e) {
			System.out.println("That's not a valid input. Returning to menu");
		}
		
		return stock;
	}
	
	//modify the name method
	public static ArrayList<Items> replaceName(ArrayList<Items> stock, String oldname, String newname){
		for(Items i : stock) {
			if (i.getName().equalsIgnoreCase(oldname)) {
			i.setName(newname);
			}
		}
		
		return stock;
	}
	//modify price method
	public static ArrayList<Items> replacePrice(ArrayList<Items> stock, String name, double newprice){
		for(Items i : stock) {
			if (i.getName().equalsIgnoreCase(name)) {
				i.setPrice(newprice);
			}
		}
		return stock;
	}
	
	//modify date method
	public static ArrayList<Items> replaceDate(ArrayList<Items> stock, String name, String datetime){
		String[] date = datetime.split("/");
		for(Items i : stock) {
			if (i.getName().equalsIgnoreCase(name)) {
				ProduceItem p = (ProduceItem) i;
				p.setYear(Integer.parseInt(date[2]));
				p.setMonth(Integer.parseInt(date[0]));
				p.setDay(Integer.parseInt(date[1]));
			}
		}
		return stock;
	}

	//modify age
	public static ArrayList<Items> replaceAge(ArrayList<Items> stock, String name, int age){
		
		for(Items i : stock) {
			if (i.getName().equalsIgnoreCase(name)) {
				AgeRestrictedItem p = (AgeRestrictedItem) i;
				p.setAgeRestriction(age);
			}
		}
		return stock;
	}
	//sorting method
	public static ArrayList<Items> sort(ArrayList<Items> stock){
		boolean done = false;
		//use bubble sort method
		do {
			done = true;
			for (int i=0; i<stock.size() -1 ; i++) {
				if (stock.get(i+1).getName().compareTo(stock.get(i).getName())<0 ) {//if 'i+1's name is alphabetically in front of i
					Items temp = stock.get(i+1);//change the i and i+1 's position					
					stock.remove(i+1);
					stock.add(i, temp);
					done = false;
				}
			}
		}while(!done);		
		
		return stock;
	}
	
	//create save file method
	public static void saveFile(ArrayList<Items> stock, Scanner input) {
		System.out.print("Save file: ");
		String filename = input.nextLine();
		//sort ArrayList before save it
		stock = sort(stock);
		
		
		try {
			FileWriter writer = new FileWriter(filename);
			
			for(Items i : stock) {
				writer.write(i.toCSV()+ "\n");
				writer.flush();
			}
			
			writer.close();
		}catch (IOException e) {
			System.out.println("Error saving to file.");
		}
		
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file: ");
		String filename = input.nextLine();
		//call method to read the file
		ArrayList<Items> stock = readData(filename);
		//call the menu method
		stock = menu(stock);
		//call method to save details
		input.close();
	}

}
