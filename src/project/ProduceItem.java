package project;
//Chunxin Luo  04/29/2022  create a subclass produceItems
public class ProduceItem extends Items {
	
	private int year;
	private Month month;
	private int day;
	//private Date expirationDate, was trying to use this but need to figure out
	
	//default constructor
	public ProduceItem() {
		super();
		year = 1970;
		month = Month.JANUARY;
		day = 1;
		//expirationDate = Date.parseDate("1970/01/01");
	}
	//non-default constructor
	public ProduceItem(String name , double price, int year, int month, int day) {
		super(name, price);
		this.year = year;
		this.month = Month.getMonth(month);
		this.day = day;
		
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(int month) {
		Month m = Month.getMonth(month);
		if (m != null) {
		this.month = m;
		}
	}
	
	public void setMonth(Month month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day >=1 && day <= month.numberOfDays()) {
		this.day = day;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() + ", the expration date is " 
	+ month.number() + "/" + day + "/" + year;
	}
	
	public String dateToString() {
		return month.number() + "/" + day + "/" + year;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}else if (obj == null) {
			return false;
		}else if (!(obj instanceof ProduceItem)) {
			return false;
		}
		
		ProduceItem p = (ProduceItem) obj;
		if (!super.equals(obj)) {
			return false;
		}else if (year != p.getYear()){
			return false;
		}else if (month != p.getMonth()) {
			return false;
		}else if (day != p.getDay()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	protected String csvData() {
		return super.csvData() +"," + month.number() + "/" + day + "/" + year;
	}
	
}
