package project;
//Chunxin Luo  04/29/2022  create a subclass ageRestrictedItems
public class AgeRestrictedItem extends Items {
	
	private int AgeRestriction;
	//default constructor
	public AgeRestrictedItem() {
		super();
		AgeRestriction = 0;
	}
	//non-default constructor
	public AgeRestrictedItem(String name, double price, int age) {
		super(name, price);
		this.AgeRestriction = age;
	}

	public int getAgeRestriction() {
		return AgeRestriction;
	}

	public void setAgeRestriction(int ageRestriction) {
		if(ageRestriction>0) {
		this.AgeRestriction  = ageRestriction;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeRestrictedItem)) {
			return false;
		}
		
		AgeRestrictedItem a = (AgeRestrictedItem) obj;
		if(this.AgeRestriction != a.getAgeRestriction()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " has the age restriction of " + AgeRestriction;
	}
	
	@Override
	protected String csvData() {
		return super.csvData() +"," + AgeRestriction;
	}
	

}
