package project;
//Chunxin Luo  04/29/2022  create a subclass shelvedItems
public class ShelvedItem extends Items {

	//non-default constructor
	public ShelvedItem() {
		super();
	}
	//non-default constructor
	public ShelvedItem(String name, double price) {
		super(name, price);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ShelvedItem)) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString(); 
	}
	
	@Override
	protected String csvData() {
		return super.csvData();
	}
	

}
