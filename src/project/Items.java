package project;
//Chunxin Luo  04/29/2022  create a superclass Items
public class Items {
	
	protected String name;
	private double price;

	//default constructor
	public Items() {
		name = "name";
		price = 0.0;
	}
	//non-default constructor
	public Items(String name, double price) {
		this.name = name;
		this.price = price;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if(price >= 0.0) {
		this.price = price;
		}
	}
	
	public String toString() {
		String result = "Stock: " + name;
		result += ", the price is " + price;
		
		return result;
	}
	
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}else if (this == obj) {
			return true;
		}else if (!(obj instanceof Items)) {
			return false;
		}
		
		Items i = (Items) obj;
		if (!name.equals(i.getName())) {
			return false;
		}else if ( price != i.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	protected String csvData() {
		return name + "," + price;
	}
	
	public String toCSV() {
		return csvData();
	}
	

}
